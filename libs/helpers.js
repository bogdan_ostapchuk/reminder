const createStorage = (storageName) => {
    if ( !localStorage.getItem( storageName ) ) {
        localStorage.setItem( storageName, "{}" );
    }
};

const templateEngine = (tpl, data) => {
    var regexp = /<%([^%>]+)?%>/g,
        match;

    while (match = regexp.exec(tpl)) {
        tpl = tpl.replace(match[0], data[match[1]])
    }

    return tpl;
};

const getParentByTagName = (node, tagname) => {
    var parent;
    if (node === null || tagname === '') return;
    parent = node.parentNode;
    tagname = tagname.toUpperCase();

    while (parent.tagName !== "HTML") {
        if (parent.tagName === tagname) {
            return parent;
        }
        parent = parent.parentNode;
    }

    return parent;
};

export {
    createStorage,
    templateEngine,
    getParentByTagName
}