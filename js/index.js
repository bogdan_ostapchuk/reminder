import { createStorage, templateEngine, getParentByTagName } from '../libs/helpers';
import toastr from 'toastr';

const Reminder = function( options ) {
    const instance = this;
    const container = options.elem;
    
    /*--------------form elems----------*/
    const form = document.querySelector('#event-form');
    const eventTitle = document.querySelector('#event_title');
    const eventDate = document.querySelector('#event_date');
    const eventTime = document.querySelector('#event_time');
    const eventContainer = document.querySelector('#event_list');
    const _template   = document.querySelector("#event_tmpl").innerHTML;
    /*--------------form elems----------*/
    
    const storageName = `reminder.${container.id}`;
    const localStorageData = JSON.parse( localStorage.getItem( storageName ));

    init();

    function init() {
        instance.showEvents(storageName, _template, eventContainer);
        createStorage(storageName);

        form.addEventListener( "submit", onSubmit );
        form.reset();
    }

    function onSubmit(e) {
        e.preventDefault();

        instance.addEvent({eventTitle: eventTitle.value, eventDate: eventDate.value, eventTime: eventTime.value}, storageName);

        eventTitle.value = '';
        eventDate.value = '';
        eventTime.value = '';
    }

    eventContainer.addEventListener('click', function (event) {
        var parentLiNode = getParentByTagName(event.target, 'LI'),
            idAttr = parentLiNode.getAttribute('id');

        if(event.target.classList[0] == 'updateEvent') instance.updateEvent(localStorageData, storageName, idAttr, 'true');
        if(event.target.classList[0] == 'delete') instance.deleteEvent(localStorageData, storageName, idAttr);
    });
};

Reminder.prototype.addEvent = (event, storageName) => {
    let localStorageData = JSON.parse( localStorage.getItem( storageName ) );

    localStorageData[Date.now()] = {
        'title': event.eventTitle,
        'datetime': `${event.eventDate} - ${event.eventTime}`,
        'subscribe': 'false'
    };

    localStorage.setItem( storageName, JSON.stringify( localStorageData ) );

    location.reload();
};

Reminder.prototype.deleteEvent = function(localStorageData, storageName, itemId) {
    delete localStorageData[itemId];

    localStorage.setItem( storageName, JSON.stringify( localStorageData ) );

    location.reload();
};

Reminder.prototype.updateEvent = function(localStorageData, storageName, itemId, newVal) {
    localStorageData[itemId].subscribe = newVal;

    localStorage.setItem( storageName, JSON.stringify( localStorageData ) );

    toastr.success('Subscribe successfully!');
};

Reminder.prototype.showEvents = function(storageName, tpl, domElem) {
    var localStorageData = JSON.parse( localStorage.getItem( storageName ));

    for(var key in localStorageData) {
        if(localStorageData.hasOwnProperty( key )) {
            domElem.innerHTML += templateEngine(tpl, {
                _id_: key,
                _title_: localStorageData[key]['title'],
                _date_: localStorageData[key]['datetime']
            });
        }
    }
};

new Reminder({
    elem: document.getElementById( "container" )
});
